#include <OneWire.h>
#include <DallasTemperature.h>
#define One_Wire_Bus 8
OneWire onewire(One_Wire_Bus);
DallasTemperature sensors(&onewire);
void setup() {
  Serial.begin(9600);
  sensors.begin();
}

void loop() {
  sensors.requestTemperatures();
  Serial.print("The temperature is:");
  Serial.println(sensors.getTempCByIndex(0));
  delay(500);
}
