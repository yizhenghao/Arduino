#include <OneWire.h>
#include <DallasTemperature.h>
#define One_Wire_Bus 8
OneWire onewire(One_Wire_Bus);
DallasTemperature sensors(&onewire);
void setup() {
  Serial.begin(9600);
  sensors.begin();
}
int temp;
int new;
void loop() {
  new = sensors.getTempCByIndex(0);
  while (new == temp) {
    sensors.requestTemperatures();
    temp = new;
    Serial.print("The temperature is:");
    Serial.println(temp);
    delay(500);
    new=sensors.getTempCByIndex(0);
  }
}
